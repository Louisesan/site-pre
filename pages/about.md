---
layout: "header"
title: "Rozenn Uguen "
permalink: /about/
images :
- "/img/chap22-4.jpeg"
---

### <a href="/index.html">retour </a>
###### rozenn.uguen@gmail.com <br> instagram : @roze.zen <br> à bientôt =)


#### formations
##### 2021 - 2022 <span class="des">École pour devenir prof, Toulouse (obtention du concours)</span>
##### 2019 - 2021 <span class="des">ESAAT option Design Graphique, Roubaix</span>
##### 2017 - 2019 <span class="des">BTS design graphique, Lycée Jean-Pierre Vernant, Sèvres</span>

#### <br> enseignements
##### 2021 - 2022 <span class="des">Professeure DNMADE, Lycée Les Arènes, Toulouse</span>
##### 2020 - 2021 <span class="des">Professeure BTS, Lycée Les Arènes, Toulouse</span>

#### <br> événements
##### 2022 <span class="des">Les puces de l'illustration, Bagnolet</span>
##### 2022 <span class="des">Festival Saint-Germains-des-Prints avec Pré.faces, Paris</span>

#### <br> ateliers
##### 2022 <span class="des">Atelier sérigraphie au festival le cHap, Bretagne </span>
##### 2021 <span class="des">Atelier sérigraphie au festival le cHap, Bretagne</span>
