---
layout: "project"
title: "peinture numérique"
permalink: /num/
images:
  - "/img/peinturenum2.jpeg"
  - "/img/peinturenum1.jpeg"
  - "/img/peinturenum3.jpeg"


---
<div id="title">
<a href="/illustration/">◊ illustration</a> <a href="/index.html"><h3>retour</h3></a>
</div>

<div id="containtitre">
<a href="/gouache/">peinture à la gouache </a> &emsp;
<a href="/riso/">risographie </a>&emsp;
<a href="/seri/">sérigraphie </a>&emsp;
<mark> <a href="/num/">peinture numérique</a> </mark>
</div>
