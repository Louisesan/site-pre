---
layout: "default"
title: "Rozenn Uguen"
permalink: /index.html
images:
- "/img/peinture1.jpeg"
- "/img/peinture2.jpeg"
- "/img/porcelaine1.jpeg"
- "/img/riso1.jpeg"
- "/img/peinture3.jpeg"
- "/img/porcelaine4.jpeg"
- "/img/porcelaine6.jpeg"
- "/img/porcelaine3.jpeg"
- "/img/peinture4.jpeg"
- "/img/peinture5.jpeg"
- "/img/porcelaine2.jpeg"
- "/img/riso2.jpeg"
- "/img/peinturenum2.jpeg"
- "/img/peinture6.jpeg"
- "/img/peinture7.jpeg"
- "/img/porcelaine5.jpeg"
- "/img/peinturenum1.jpeg"
- "/img/porcelaine7.jpeg"
- "/img/peinture8.jpeg"
- "/img/peinture9.jpeg"
- "/img/peinturenum3.jpeg"
- "/img/porcelaine8.jpeg"
---

# <a href="about"> ◊ à propos  </a>

# <a href="/porcelaine/"> peinture sur porcelaine&emsp;◊ </a>
# <a href=""> les tapis&emsp;◊ </a>
# <a href="/chap22/"> graphisme&emsp;◊ </a>
# <a href="/illustration/"> illustration &emsp;◊</a>
