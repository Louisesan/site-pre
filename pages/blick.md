---
layout: "graphpro"
title: "Blick Blick"
permalink: /blick/
images:
  - "/img/blick.jpeg"
  - "/img/blick2.jpeg"
  - "/img/blick3.jpeg"
  - "/img/blick4.jpeg"
  - "/img/blick5.jpeg"
  - "/img/blick6.jpeg"


---
<div id="title">
◊ graphisme <a href="/index.html"><h3>retour</h3></a>
</div>

<div id="containtitre">
<a href="/chap22/"> le cHap 2022 &emsp; </a>
<mark> <a href="/blick/"> Blick Blick  </mark> &emsp; </a>
<a href="/boum/"> Boum Boum joli chaos frénétique &emsp; </a>
<a href="/chap19/"> le cHap 2019 &emsp; </a>
<a href="/carte/"> cartes pré.faces &emsp; </a>
<a href="/switch/"> festival Swicth </a>
</div>

<div id="description">
Affiche réalisée pour le festival le cHap. Nous y avons réalisé un atelier d'initiation à la sérigraphie avec pré.faces. Nous avons proposé au festivaliers de sérigraphier des motifs, soit sur des tissus récupérés au préalable, soit sur leur vêtement directement.
</div>
