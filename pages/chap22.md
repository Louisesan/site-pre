---
layout: "graphpro"
title: "le cHap 2022"
permalink: /chap22/
images:
  - "/img/chap.jpeg"
  - "/img/graphisme1.jpeg"
  - "/img/chap22-4.jpeg"
  - "/img/chap22-3.jpeg"
  - "/img/chap22-2.jpeg"
  - "/img/chap22-6.jpeg"




---
<div id="title">
◊ graphisme <a href="/index.html"><h3>retour</h3></a>
</div>

<div id="containtitre">
<mark> <a href="/chap22/"> le cHap 2022  </mark> &emsp; </a>
<a href="/blick/"> Blick Blick &emsp; </a>
<a href="/boum/"> Boum Boum joli chaos frénétique &emsp; </a>
<a href="/chap19/"> le cHap 2019 &emsp; </a>
<a href="/carte/"> cartes pré.faces &emsp; </a>
<a href="/switch/"> festival Swicth </a>
</div>

<div id="description">
Affiche réalisée pour le festival le cHap. Nous y avons réalisé un atelier d'initiation à la sérigraphie avec pré.faces. Nous avons proposé au festivaliers de sérigraphier des motifs, soit sur des tissus récupérés au préalable, soit sur leur vêtement directement.
</div>
