---
layout: "project"
title: "illustration"
permalink: /illustration/
images:
  - "/img/peinture1.jpeg"
  - "/img/peinture2.jpeg"
  - "/img/riso1.jpeg"
  - "/img/peinture3.jpeg"
  - "/img/peinture4.jpeg"
  - "/img/peinture5.jpeg"
  - "/img/riso2.jpeg"
  - "/img/peinturenum2.jpeg"
  - "/img/peinture6.jpeg"
  - "/img/peinture7.jpeg"
  - "/img/peinturenum1.jpeg"
  - "/img/peinture8.jpeg"
  - "/img/peinture9.jpeg"
  - "/img/peinturenum3.jpeg"


---
<div id="title">
◊ illustration <a href="/index.html"><h3>retour</h3></a>
</div>

<div id="containtitre">
<a href="/gouache/">peinture à la gouache </a> &emsp; <a href="/riso/">risographie </a>&emsp; <a href="/seri/">sérigraphie </a>&emsp; <a href="/num/">peinture numérique</a>
</div>
