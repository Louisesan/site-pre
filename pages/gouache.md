---
layout: "project"
title: "peinture à la gouache"
permalink: /gouache/
images:
  - "/img/peinture1.jpeg"
  - "/img/peinture2.jpeg"
  - "/img/peinture3.jpeg"
  - "/img/peinture4.jpeg"
  - "/img/peinture5.jpeg"
  - "/img/peinture6.jpeg"
  - "/img/peinture7.jpeg"
  - "/img/peinture8.jpeg"
  - "/img/peinture9.jpeg"


---
<div id="title">
<a href="/illustration/">◊ illustration</a> <a href="/index.html"><h3>retour</h3></a>
</div>

<div id="containtitre">
<mark> <a href="/gouache/">peinture à la gouache </mark></a> &emsp;
<a href="/riso/">risographie </a>&emsp;
<a href="/seri/">sérigraphie </a>&emsp;
<a href="/num/">peinture numérique</a>
</div>
